package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Jackhoang14" ) );
	}
	
	@Test
	public void testIsValidLoginExceptional( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "4jack" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "jackh4" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "555555" ) );
	}
	
	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "jack1423232" ));
	}
	
	@Test
	public void testIsValidLoginLengthExceptional( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "jac.12334" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "JackH1" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "jack5" ) );
	}
	
	


}
