package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		String regex = "^[a-zA-Z][a-zA-Z0-9]{5,}";
		return loginName.matches(regex);
	}
	
}
